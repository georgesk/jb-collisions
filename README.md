Jeu en 2D, pluie d'objets
=========================

auteurs : **Nafaa, Julien, Anthony**


Résumé du projet : 
------------------

Un véhicule se déplace en bas de l'écran, une pluie d'objets 
contondants s'abat dessus, le véhicule doit éviter les collisions.

Buts d'étapes :
---------------

### 02/03 ###


 *   créer un modèle 3D de véhicule, en faire des vues 2D qui seront présentées à l'écran.
 *   revoir l'organisation du groupe afin que trois fassent plus de travail qu'un seul à la fois
 *   le but est atteint si 2 vues au moins du véhicule sont exportées et que le groupe sait partager les travaux.

### 09/03 ###

 *   vérifier que pygame saura fonctionner sous Windows comme sous Linux
 *   maîtriser le test de collision de deux objets
 *   le but est atteint si on a un programme avec des objets mobiles, une collision et si le programme produit un évènement lors de la collision
 
 **But atteint**, tag "v03.09"
 
