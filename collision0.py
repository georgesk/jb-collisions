﻿import pygame

pygame.init()

window = pygame.display.set_mode((800,600))

pygame.display.set_caption("Collision Detection")

black = (0,0,0)
white = (255,255,255)
red = (255,25,25)

clock = pygame.time.Clock()

class Sprite:
    """
    Classe pour implémenter un objet mobile qui se dessine à l'écran.
    La méthode render est ici très rudimentaire, ça dessine juste un rectangle
    plus ou moins coloré
    """
    def __init__(self,x,y,width,height):
        """
        Le constructeur
        :param x: abscisse intiale
        :param y: ordonnée initiale
        :param width: largeur
        :param height: hauteur
        """
        self.x=x
        self.y=y
        self.width=width
        self.height=height

    # le décorateur @property permet de faire un appel objet.hitboxes
    # plutôt que l'appel plus long objet.hitboxes()
    @property
    def hitboxes(self):
        """
        La liste des zones de collision propre à l'objet
        Ces zones de collision sont des reactangles de type pygame.Rect
        :return: une liste de boîtes
        :rtype: list(pygame.Rect, ...)
        """
        return [pygame.Rect(self.x, self.y, self.width, self.height)]

    def detecteCollision(self, other):
        """
        détection de collisions
        :param other: un autre objet mobile
        :type other: Sprite
        :return: vrai si n'importe quelle hitbox de l'un des objets touche
                 n'importe quelle hitbox de l'autre objet
        :rtype: bool
        """
        for hb1 in self.hitboxes:
            for hb2 in other.hitboxes:
                if hb1.colliderect(hb2): return True
        return False

    def render(self,collision):
        """
        dessin de l'objet à l'écran
        :param collision: détection d'une collision
        :type collision: bool
        """
        if (collision==True):
            pygame.draw.rect(window,red,(self.x,self.y,self.width,self.height))
        else:
            pygame.draw.rect(window,black,(self.x,self.y,self.width,self.height))

Sprite1=Sprite(100,50,100,100)
Sprite2=Sprite(300,50,100,100)

moveX,moveY=0,0

gameLoop=True
while gameLoop:
    for event in pygame.event.get():
        if (event.type==pygame.QUIT):
            gameLoop=False
        if (event.type==pygame.KEYDOWN):
            if (event.key==pygame.K_LEFT):
                moveX = -4
            if (event.key==pygame.K_RIGHT):
                moveX = 4
            if (event.key==pygame.K_UP):
                moveY = -4
            if (event.key==pygame.K_DOWN):
                moveY = 4
        if (event.type==pygame.KEYUP):
            if event.key in (pygame.K_LEFT, pygame.K_RIGHT):
                moveX=0
            if event.key in (pygame.K_UP, pygame.K_DOWN):
                moveY=0
    window.fill(white)
    Sprite1.x+=moveX
    Sprite1.y+=moveY
    collisions=Sprite1.detecteCollision(Sprite2)
    Sprite1.render(collisions)
    Sprite2.render(False)
    pygame.display.flip()
    clock.tick(50)

pygame.quit()
